## RTU documentation

### Request a test execution from RTU
```mermaid
sequenceDiagram
    participant FMS
    participant RunTest as Coordinator.Scheduling.Scheduler.RunTest 
    
    FMS->>+RunTest: Request baseline
        RunTest->>RunTest: Load baseline
        RunTest->>RunTest: Load nulling
        RunTest->>RunTest: Execute test
        RunTest->>RunTest: Other steps
    RunTest-->>-FMS: Baseline measure + Exceptions
```

#### Load baseline 
```mermaid
sequenceDiagram
    participant RunTest as Coordinator.Scheduling.Scheduler.RunTest 
    participant LoadBaseLine as Coordinator.Results.Results.LoadBaseLine 
    participant FindBaseline as Messaging.BaselineLoader.BaselineLoaderImplem.FindBaseline
    participant FindLatestBaseline as Common.DataStore.DataStoreService.FindLatestBaseline
    participant db as Local DB
    participant RequestBaselineFromFms as Messaging.BaselineLoader.BaselineLoaderImplem.RequestBaselineFromFms
    participant FMS DB as FMS DB
    participant ExtractBaseline as Messaging.BaselineLoader.BaselineLoaderImplem.ExtractBaseline

    RunTest->>+LoadBaseLine: Load baseline
        LoadBaseLine->>+FindBaseline: Find baseline
            FindBaseline->>+FindLatestBaseline: Find latest baseline
                FindLatestBaseline->>+db: SQL query
                db-->>-FindLatestBaseline: Baseline or null
            FindLatestBaseline-->>-FindBaseline: Baseline or null
alt Baseline found from local DB
        FindBaseline-->>LoadBaseLine: Baseline
else Baseline not found from local DB
            FindBaseline->>+RequestBaselineFromFms: Request baseline
                RequestBaselineFromFms->>+FMS DB: Request baseline
                FMS DB-->>-RequestBaselineFromFms: Baseline or exception
            RequestBaselineFromFms-->>-FindBaseline: Baseline or exception
            FindBaseline->>+ExtractBaseline: Extract baseline 
            ExtractBaseline-->-FindBaseline: Baseline or null
        FindBaseline-->>-LoadBaseLine: Baseline or null
end
    LoadBaseLine-->>-RunTest: Baseline or null

```

#### Load nulling 
```mermaid
sequenceDiagram
    participant RunTest as Coordinator.Scheduling.Scheduler.RunTest 
    participant LoadNulling as Coordinator.Results.Results.LoadNulling 
    participant FindLatestNulling as Common.DataStore.DataStoreService.FindLatestNulling
    participant db as Local DB

    RunTest->>+LoadNulling: Load nulling
        LoadNulling->>+FindLatestNulling: Find nulling
                FindLatestNulling->>+db: SQL query for nulling or baseline if no nulling found
                db-->>-FindLatestNulling: Nulling or baseline or null
        FindLatestNulling-->>-LoadNulling: Nulling or null
    LoadNulling-->>-RunTest: Nulling or null
```

#### Execute test 
```mermaid
stateDiagram
    direction TB
    set_OtdrServiceProvider : Set OtdrServiceProvider
    LoadMeasurement : Load measurement (baeseline + nulling)
    GetOtdrMeasurementParameters : Get Otdr measurement parameters
    SetPonXplorer : Set PonXplorer based on olmMeasurement.MeasurementType 
    LoadMeasurementTestConfig : Load Test.MeasurementTestConfig as olmMeasurement 
    LoadTestParams : Load Test.TestParams if it isn't set
    SetPonXplorer2 : Set PonXplorer to false if measurement is iOLM standard
    MergeTestParams : Merge some TestParams with current test setup
    SetMeasurementType : Set MeasurementType 
    MergeTestParamsPON : set GeoPosition, combine measurement type, portId, add testParamsSetup metadata IDs
    AddMetadataIDs : Add olmMeasurement metadata IDs
    SetCategoryAndoperation1 : Set current category/operation based on ponXplorerCurrentTestMeasurementType
    SetCategoryAndoperation2 : Set current category/operation based on testParamsSetup.MeasurementType
    OlmLineTestConfiguration : Configure an OlmLineTestConfiguration object
    OlmLineTest : Configure an olmLineTest object
    ContainsTimedSource : Run olmLineTest.ExecuteSourceModeTest
    SetWavelength : Set wavelength
    TestLineOtdr : Run olmLineTest.TestLineOtdr
    GetTestLineOtdrMeasurement : Run olmLineTest.GetTestLineOtdrMeasurement
    UpdateOlmMeasurement : Update olmMeasurement.MeasurementType in case Test.TestCategory is Adhoc<br>Or SetMonitoringParams in case Test.TestCategory is Monitoring
    Get_last_adhoc_measurement : Get last adhoc olm measurement and run<br> olmLineTest.TestLine with lastAdhocOlmMeasurement
    RunTestLineWithOlmmeasurement : Run olmLineTest.TestLine with olmMeasurement
    RunTestLineWithoriginalOlmMeasurementParam : Run olmLineTest.TestLine with originalOlmMeasurementParam
    RunTestLineWithOlmmeasurement2 : Run olmLineTest.TestLine with olmMeasurement
    GetTestLineMeasurementException : Get TestLine Measurement/Exception
    SearchForBaselineLocally : Search for the baseline locally. If not found, wait 1s and check again until 6s or cancellation
    Add_measurement/exceptions : Add measurement/exceptions to a testContext object 

    note left of LoadMeasurement : Loading using OlmStorage class
    note right of SetPonXplorer : Used to differenciate btw PON and P2P
    note left of SetCategoryAndoperation1 : it covers the following categories/operations <br>Basline/[EditBaseline,RecoverBaseline,Baseline]<br> Measurement/[RLNulling,AdHocRD,AnchorRD,FastOnDemand,OnDemand]
    note left of SetCategoryAndoperation2 : it covers the following categories/operations <br>Measurement/[Monitoring,Baseline]


    state PonXplorer <<choice>>
    state PonXplorer2 <<choice>>
    state TimedSource <<choice>>
    state otdrMeasurementParameters <<choice>>
    state AutoBaselineTest <<choice>>
    state AutoBaselineTestValid <<choice>>
    state PonXplorer3 <<choice>>
    state AnchorRD <<choice>>
    state OperationEditBaseline <<choice>>
    state measurementException <<choice>>
    state cancelled <<choice>>
    state PonXplorer4 <<choice>>

    state LoadTestParams {
        Loading --> SetPonXplorer2
    }
    state MergeTestParams {
        PonXplorer --> MergeTestParamsPON: PonXplorer is true
        PonXplorer --> SetMeasurementType: PonXplorer is false
    }
  

    [*] --> set_OtdrServiceProvider
    set_OtdrServiceProvider --> LoadMeasurement
    LoadMeasurement --> GetOtdrMeasurementParameters
    GetOtdrMeasurementParameters --> LoadMeasurementTestConfig
    LoadMeasurementTestConfig --> SetPonXplorer
    SetPonXplorer --> LoadTestParams
    LoadTestParams -->  MergeTestParams
    MergeTestParams --> AddMetadataIDs
    AddMetadataIDs --> PonXplorer2
    PonXplorer2 --> SetCategoryAndoperation1 : PonXplorer is true
    PonXplorer2 --> SetCategoryAndoperation2: PonXplorer is false
    SetCategoryAndoperation1 --> OlmLineTestConfiguration
    SetCategoryAndoperation2 --> OlmLineTestConfiguration
    OlmLineTestConfiguration --> OlmLineTest
    OlmLineTest --> TimedSource
    OlmLineTest --> otdrMeasurementParameters
    TimedSource --> SetWavelength: testParamsSetup != null and <br>testParamsSetup.MeasurementType contains TimedSource
    SetWavelength --> ContainsTimedSource
    ContainsTimedSource --> ReleaseAllOpticalSwitches 
    ReleaseAllOpticalSwitches --> [*]
    otdrMeasurementParameters --> TestLineOtdr : otdrMeasurementParameters != null
    TestLineOtdr --> GetTestLineOtdrMeasurement
    GetTestLineOtdrMeasurement --> [*]
    OlmLineTest --> AutoBaselineTest 
    AutoBaselineTest --> Load_Baseline : AutoBaselineTest is true
    Load_Baseline --> Check_if_it's_valid_for_autobaseline
    Check_if_it's_valid_for_autobaseline --> AutoBaselineTestValid
    AutoBaselineTestValid  --> Update_local_DB : Not valid
    Update_local_DB --> [*]
    OlmLineTest --> UpdateOlmMeasurement
    UpdateOlmMeasurement --> PonXplorer3
    PonXplorer3 --> AnchorRD : PonXplorer is true
    PonXplorer3 --> RunTestLineWithOlmmeasurement : PonXplorer is false
    AnchorRD --> Get_last_adhoc_measurement : currentOperation == Operation.AnchorRD
    AnchorRD --> OperationEditBaseline : anchorRdMode is false
    OperationEditBaseline --> RunTestLineWithoriginalOlmMeasurementParam : currentOperation == Operation.EditBaseline
    OperationEditBaseline --> RunTestLineWithOlmmeasurement2 : else

    RunTestLineWithOlmmeasurement2 --> GetTestLineMeasurementException
    RunTestLineWithoriginalOlmMeasurementParam --> GetTestLineMeasurementException
    Get_last_adhoc_measurement --> GetTestLineMeasurementException
    RunTestLineWithOlmmeasurement --> GetTestLineMeasurementException

    GetTestLineMeasurementException --> measurementException
    measurementException --> SearchForBaselineLocally : measurementException != null <br>&& Test.TestCategory == TestContext.TestCategories.Monitoring
    SearchForBaselineLocally --> cancelled
    measurementException --> cancelled
    cancelled -->  Release/dispose_ressources: If cancelled
    cancelled --> Add_measurement/exceptions
    Release/dispose_ressources -->[*]
    Add_measurement/exceptions --> PonXplorer4
    PonXplorer4 --> ManagePONResults : PonXplorer is true
    PonXplorer4 --> ManageP2PResults : PonXplorer is false
    ManagePONResults --> [*]
    ManageP2PResults --> [*]
```

#### Manage PON results
```mermaid
stateDiagram
    direction TB

    checkForFaultInMeasure : Check for fault in measure
    Create_save_results : Create results and save binaries in database
    saveArgumentMeasure : Save ArgumentMeasure using OlmStorage class
    UpdateLocalBaseline : Update InvalidForAutobaseline field to false for the last locally saved baseline 
    InsertRawBaseline : Insert new baseline to the local DB
    UpdateLocalBaseline1 : Update SentToFMS field to true for the last locally saved baseline 
    deleteNulling : Delete nulling because it is the same mesure as the baseline
    loadLatestMonitoring : Load the latest monitoring from local DB
    InsertRawNulling : Insert new nulling to the local DB with the last monitoring entry ID
    saveArgumentMeasure2 : Save ArgumentMeasure using OlmStorage class
    UpdateLocalNulling : Update SentToFMS field to true for the last locally saved nulling 
    saveArgumentMeasure3 : Save ArgumentMeasure using OlmStorage class
    InsertRawNulling2 : Insert new nulling to the local DB
    AddResults1 : AddResults
    AddResults2 : AddResults
    UpdateLocalNulling2 : Update SentToFMS field to true for the last locally saved nulling <br>and execute addResults in case it was set to false
    AddResults3 : AddResults
    AddResults4 : AddResults
    saveArgumentMeasure4 : Save ArgumentMeasure using OlmStorage class
    InsertAdhocs : Insert Adhocs to the local DB
    AddResults5 : AddResults
    InsertMonitoring : Insert Monitoring results to the local DB
    AddResults6 : AddResults
    InsertMonitoring2 : Insert Monitoring results to the local DB
    AddExtraResults : Add extra results for fast monitoring
    AddExtraErrorResults : Add extra error results 

    state measurementException <<choice>>
    state AnchorRD <<choice>>
    state currentOperation <<choice>>
    state nullingOutputInput <<choice>>
    state currentOperationForNulling1 <<choice>>
    state currentOperationForNulling2 <<choice>>
    state currentOperationForNulling3 <<choice>>
    state RLNulling <<choice>>
    state currentOperationForMeasure1 <<choice>>
    state measurementExceptionMeasure <<choice>>
    
    state join_state <<fork>>
    state fork_state_MBN <<fork>>
    state join_state_MBN <<join>>
    
    state Baseline {
        AnchorRD --> saveArgumentMeasure : currentOperation == AnchorRD
        AnchorRD --> InsertRawBaseline : else
        saveArgumentMeasure --> UpdateLocalBaseline
        InsertRawBaseline --> join_state
        UpdateLocalBaseline --> join_state
        join_state --> currentOperation : currentOperation is Baseline|EditBaseline|RecoverBaseline|RLNulling
        currentOperation --> UpdateLocalBaseline1
        UpdateLocalBaseline1 --> AddResults1

    }

    state Nulling {
        nullingOutputInput --> currentOperationForNulling1: output nulling
            currentOperationForNulling1 -->  deleteNulling : currentOperation is Baseline|EditBaseline|RecoverBaseline
            currentOperationForNulling1 --> currentOperationForNulling2 : else
                currentOperationForNulling2 --> loadLatestMonitoring : currentOperation is Monitoring|OnDemand|FastOnDemand
                    loadLatestMonitoring --> saveArgumentMeasure2
                    saveArgumentMeasure2 --> InsertRawNulling
                    InsertRawNulling --> currentOperationForNulling3
                    currentOperationForNulling3 --> UpdateLocalNulling : currentOperation is OnDemand|FastOnDemand
                currentOperationForNulling2 --> saveArgumentMeasure3:else
                    saveArgumentMeasure3 --> InsertRawNulling2
       
                InsertRawNulling2 --> RLNulling
                UpdateLocalNulling --> RLNulling
                RLNulling --> AddResults2 : currentOperation is RLNulling

        nullingOutputInput --> UpdateLocalNulling2: input nulling <br>and UploadData is true <br>and ( currentOperation is not monitoring <br>or (alreadyKnownFault is false AND currentTestContextHasFault is true) )
    }

    state Measure {
        currentOperationForMeasure1 --> AddResults3 : currentOperation is AnchorRD
        currentOperationForMeasure1 --> AddResults4 : currentOperation is AdHocRD
            AddResults4 --> measurementExceptionMeasure
            measurementExceptionMeasure --> saveArgumentMeasure4 : measurementException == null
            saveArgumentMeasure4 --> InsertAdhocs
        currentOperationForMeasure1 --> AddResults5 : currentOperation is Monitoring
            AddResults5 --> InsertMonitoring
        currentOperationForMeasure1 --> AddResults6 : currentOperation is OnDemand|FastOnDemand
            AddResults6 --> InsertMonitoring2
    }

    note left of measurementException : Added to fix bug FG-5688

    [*] --> measurementException
    measurementException --> GetOlmInstrument : measurementException == null
    GetOlmInstrument --> checkForFaultInMeasure
    checkForFaultInMeasure --> Create_save_results
    Create_save_results --> fork_state_MBN
    fork_state_MBN --> Baseline 
    fork_state_MBN --> Nulling 
    fork_state_MBN --> Measure 
    Baseline --> join_state_MBN
    Nulling --> join_state_MBN
    Measure --> join_state_MBN
    join_state_MBN --> AddExtraResults
    AddExtraResults --> AddExtraErrorResults
    measurementException --> AddExtraErrorResults
    AddExtraErrorResults --> [*]

```

#### Manage P2P results
```mermaid
stateDiagram
    direction TB
    AddResults1 : AddResults
    CheckForFault1 : CheckForFault
    CheckForFault2 : CheckForFault
    AddResults2 : AddResults

    state measurementException <<choice>>
    state containsBaselineCondition <<choice>>
    state TestCategoryCondition <<choice>>
    state TestCategoryCondition2 <<choice>>

    [*] --> measurementException
    measurementException --> containsBaselineCondition : measurementException == null
    containsBaselineCondition --> CheckForFault1 : MeasurementType contains baseline
        CheckForFault1 --> AddResults1
    containsBaselineCondition --> CheckForFault2 : else 
        CheckForFault2 --> TestCategoryCondition 
        TestCategoryCondition --> CheckForAlreadyKnownFault: TestCategory is Monitorintg|OnDemand and status completed
            CheckForAlreadyKnownFault -->CheckIfTheTestContextHasFault
            CheckIfTheTestContextHasFault --> AddResults2
            TestCategoryCondition --> AddResults2
            AddResults2 --> TestCategoryCondition2
            TestCategoryCondition2 --> SaveLastMonitoring:TestCategories is monitoring or TOD
            SaveLastMonitoring -->AddExtraErrorResults
            TestCategoryCondition2 --> AddExtraErrorResults
    AddResults1 --> AddExtraErrorResults
    AddExtraErrorResults --> [*]
```

